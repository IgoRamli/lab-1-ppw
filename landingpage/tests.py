# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.urls import reverse

# Create your tests here.

class LandingPageTest(TestCase):
    def test_landingpage_render_success(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        
    def test_landingpage_gallery_render_success(self):
        response = self.client.get(reverse('gallery'))
        self.assertEqual(response.status_code, 200)