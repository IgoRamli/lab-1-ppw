from django.shortcuts import render
from django.http import *

from django.shortcuts import render

from .models import Event

# Create your views here.

def index(request):
	all_events = Event.objects.all()

	context = {'allEvents': all_events}

	return render(request, 'jadwal.html', context)

def add_schedule(request):
	if request.method == 'POST':
		data = request.POST

		try:
			new_event = Event(
				event_name=data['eventTitle'],
				time=data['time'],
				location=data['location'],
				category=data['category']
				)

			new_event.save()
		except:
			pass
            
	all_events = Event.objects.all()

	context = {'allEvents': all_events}

	return render(request, 'jadwal.html', context)

def del_schedule(request):
	if request.method == 'POST':
		data = request.POST

		Event.objects.filter(id=data['id']).delete()

	all_events = Event.objects.all()

	context = {'allEvents': all_events}

	return render(request, 'jadwal.html', context)