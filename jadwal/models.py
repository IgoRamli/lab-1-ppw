from django.db import models

# Create your models here.

class Event(models.Model):
	GENERAL = 'GR'
	PERSONAL = 'PR'
	CP = 'CP'
	CLASS = 'CL'

	CATEGORY_CHOICES = [
		(GENERAL, 'General'),
		(CP, 'Competitive Programming'),
		(PERSONAL, 'Personal'),
		(CLASS,  'Class'),
	]

	event_name = models.CharField(max_length=100)
	time = models.DateTimeField()
	location = models.CharField(max_length=100, default="")
	category = models.CharField(
		max_length=2,
		choices=CATEGORY_CHOICES,
		default=GENERAL)