# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.urls import reverse

from datetime import datetime

from .models import Event

# Create your tests here.

class JadwalTest(TestCase):
    def test_jadwal_render_success(self):
        response = self.client.get(reverse('jadwal-index'))
        self.assertEqual(response.status_code, 200)
        
    def test_jadwal_add_schedule_success(self):
        response = self.client.post(reverse('add-schedule'),
            {'eventTitle': 'Testing', 'time': '2002-12-12',
            'location': 'Rumah', 'category': 'GR'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Event.objects.all().count(), 1)
        self.assertEqual(Event.objects.first().time.year, 2002)
        self.assertEqual(Event.objects.first().time.month, 12)
        self.assertEqual(Event.objects.first().time.day, 12)
        
    def test_jadwal_add_schedule_fail(self):
        response = self.client.get(reverse('add-schedule'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Event.objects.all().count(), 0)
        
        response = self.client.post(reverse('add-schedule'),
            {'eventTitle': 'Testing',
            'location': 'Rumah', 'category': 'GR'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Event.objects.all().count(), 0)
        
    def test_jadwal_del_schedule_success(self):
        response = self.client.post(reverse('add-schedule'),
            {'eventTitle': 'Testing', 'time': '2002-12-12',
            'location': 'Rumah', 'category': 'GR'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Event.objects.all().count(), 1)
        
        response = self.client.post(reverse('del-schedule'), {'id': 1})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Event.objects.all().count(), 0)
        
    def test_jadwal_del_schedule_fail(self):
        response = self.client.post(reverse('add-schedule'),
            {'eventTitle': 'Testing', 'time': '2002-12-12',
            'location': 'Rumah', 'category': 'GR'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Event.objects.all().count(), 1)
        
        response = self.client.post(reverse('del-schedule'), {'id': 2})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Event.objects.all().count(), 1)
        
        response = self.client.get(reverse('del-schedule'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Event.objects.all().count(), 1)